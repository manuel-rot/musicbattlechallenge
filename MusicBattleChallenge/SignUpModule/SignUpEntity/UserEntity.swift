//
//  UserEntity.swift
//  MusicBattleChallenge
//
//  Created by Manuel Rodríguez Torres on 13/10/20.
//

import Foundation

struct UserEntity: Codable{
    var userFirstName: String?
    var userLastName: String?
    var userDescription: String?
    var userName: String?
    var password: String?
}
