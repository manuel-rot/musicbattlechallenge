//
//  SignUpViewController.swift
//  MusicBattleChallenge
//
//  Created Manuel Rodríguez Torres on 13/10/20.
//  Copyright © 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController,SignUpViewControllerProtocol {
    
    var presenter: SignUpPresenterProtocol?
    
    @IBOutlet weak var userNameTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastNameTextfield: UITextField!
    
    @IBOutlet weak var signUpButtonContainerView: CustomUIView!
    @IBOutlet weak var signUpButton: UIButton!
    
    @IBOutlet weak var usernameValidationLabel: UILabel!
    @IBOutlet weak var passwordValidationLabel: UILabel!
    @IBOutlet weak var userBioTextView: UITextView!
    
    var isImageSaved: Bool?{
        didSet{
            self.enableSignUpButton()
        }
    }
    override func viewDidLoad() {
        
        self.userNameTextfield.tag = 0
        self.passwordTextfield.tag = 1
        self.nameTextField.tag = 2
        self.lastNameTextfield.tag = 3
        
        self.userNameTextfield.delegate = self
        self.passwordTextfield.delegate = self
        self.nameTextField.delegate = self
        self.lastNameTextfield.delegate = self
        
        self.userBioTextView.delegate = self
    }
    
    @IBAction func addAPic(_ sender: Any) {
        self.presenter?.chooseImage()
    }
    @IBAction func goBackToLogin(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signUp(_ sender: Any) {
        let user = UserEntity(userFirstName: self.nameTextField.text, userLastName: self.lastNameTextfield.text, userDescription: self.userBioTextView.text, userName: self.userNameTextfield.text, password: self.passwordTextfield.text)
        self.presenter?.presentMainMenu(userData: user)
    }
    
    
    func enableSignUpButton(){
        let enable = self.usernameValidationLabel.textColor == .greenBattle && self.passwordValidationLabel.textColor == .greenBattle && self.nameTextField.text?.count != 0 && self.lastNameTextfield.text?.count != 0 && self.userBioTextView.text.count != 0 && (self.isImageSaved ?? false)
        self.signUpButton.isUserInteractionEnabled = enable
        self.signUpButtonContainerView.alpha = enable ? 1.0 : 0.5
    }
}

extension SignUpViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let newText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) else { return false }
        
        if newText == " "{ return false }
        
        self.enableSignUpButton()
        switch textField.tag {
        case 0:
            self.usernameValidationLabel.textColor = newText.count >= 4 ? .greenBattle : .lightGray
            return newText.count < 12
        case 1:
            self.passwordValidationLabel.textColor = newText.count >= 8 ? .greenBattle : .lightGray
            return newText.count < 14
        default:
            break
        }
        return true
    }
}

extension SignUpViewController: UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
    }
}

