//
//  SignUpPresenter.swift
//  MusicBattleChallenge
//
//  Created Manuel Rodríguez Torres on 13/10/20.
//  Copyright © 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class SignUpPresenter: NSObject, SignUpPresenterProtocol {

    weak var view: SignUpViewControllerProtocol?
    var interactor: SignUpInteractorProtocol?
    var router: SignUpRouterProtocol?

    private var imagePicker = UIImagePickerController()
    
    func chooseImage(){
        let alert = UIAlertController(title: nil, message: "Sube una fotografía a tu perfil", preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Tomar una foto", style: .default, handler: { action in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Galería de fotos", style: .default, handler: { action in
            self.openGallery()
        }))
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        
        self.view?.present(alert, animated: true)
    }
    
    func presentMainMenu(userData: UserEntity) {
        UserDefaults.standard.setValue((userData.userFirstName ?? "") + " " + (userData.userLastName ?? ""), forKey: "profileName")
        UserDefaults.standard.setValue(userData.userDescription, forKey: "userDescription")
        UserDefaults.standard.setValue(userData.userName, forKey: "userName")
        UserDefaults.standard.setValue(userData.password, forKey: "userPassword")
        let vc = MainMenuRouter.createModule()
        self.view?.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.view?.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    
    private func openGallery(){
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            self.view?.present(imagePicker, animated: true, completion: nil)
        }
    }
    
}

extension SignUpPresenter: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else{
            return
        }
        if let pngData = image.pngData(){
            let documents = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                let url = documents.appendingPathComponent("profile.png")
                do {
                    try pngData.write(to: url)

                    UserDefaults.standard.set(url, forKey: "userImage")

                } catch {
                    print("Unable to Write Data to Disk (\(error))")
                }
        }
        picker.dismiss(animated: true, completion: {
            self.view?.isImageSaved = true
            self.view?.showAlert(title: "", message: "Your image was successfully saved")
        })
    }
}
extension SignUpPresenter: SignUpInteractorOutputProtocol {
    
}
