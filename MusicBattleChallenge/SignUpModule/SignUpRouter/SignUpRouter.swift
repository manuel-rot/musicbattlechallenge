//
//  SignUpRouter.swift
//  MusicBattleChallenge
//
//  Created Manuel Rodríguez Torres on 13/10/20.
//  Copyright © 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class SignUpRouter: SignUpRouterProtocol {
    
    weak var presenter: SignUpPresenterProtocol?
    
    static func createModule() -> UIViewController {
        let s = mainstoryboard
        let view = s.instantiateViewController(withIdentifier: "SignUp") as! SignUpViewControllerProtocol
        var presenter: SignUpPresenterProtocol & SignUpInteractorOutputProtocol = SignUpPresenter()
        var interactor:SignUpInteractorProtocol = SignUpInteractor()
        var router: SignUpRouterProtocol = SignUpRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        router.presenter = presenter
        interactor.output = presenter
        
        return view
    }
    static var mainstoryboard: UIStoryboard {
        return UIStoryboard(name: "SignUpStoryboard", bundle: nil)
    }
}
