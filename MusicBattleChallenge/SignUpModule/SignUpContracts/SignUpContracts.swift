//
//  SignUpContracts.swift
//  MusicBattleChallenge
//
//  Created Manuel Rodríguez Torres on 13/10/20.
//  Copyright © 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
//Views
protocol SignUpViewControllerProtocol: UIViewController {
    var presenter: SignUpPresenterProtocol? {get set}
    var isImageSaved: Bool? { get set }
}
//Interactor
protocol SignUpInteractorProtocol: NSObject {
    var output: SignUpInteractorOutputProtocol? {get set}

}

protocol SignUpInteractorOutputProtocol: NSObject {

}

//Presenter
protocol SignUpPresenterProtocol: NSObject {
    var view: SignUpViewControllerProtocol? {get set}
    var interactor: SignUpInteractorProtocol? {get set}
    var router: SignUpRouterProtocol? {get set}

    func chooseImage()
    func presentMainMenu(userData: UserEntity)
    
}
//Router
protocol SignUpRouterProtocol {
    var presenter: SignUpPresenterProtocol? {get set}
    static func createModule() -> UIViewController
    
}

