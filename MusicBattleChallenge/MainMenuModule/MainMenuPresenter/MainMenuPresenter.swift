//
//  MainMenuPresenter.swift
//  MusicBattleChallenge
//
//  Created Manuel Rodríguez Torres on 13/10/20.
//  Copyright © 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class MainMenuPresenter: NSObject, MainMenuPresenterProtocol {
    var playerView: MusicPlayerViewControllerProtocol?
    
    func sendImageRequest() {
        self.view?.showLoader()
        self.interactor?.getImagesRequest()
    }
    
    
    weak var view: MainMenuViewControllerProtocol?
    var interactor: MainMenuInteractorProtocol?
    var router: MainMenuRouterProtocol?

}
extension MainMenuPresenter: MainMenuInteractorOutputProtocol {
    func onSuccessImagesResponse(imageData: ImageEntity) {
        self.view?.hideLoader()
        self.view?.imageDataArray = imageData
        self.view?.reloadCollectionView()
    }
    
    func onErrorImagesResponse(error: CodeResponse) {
        self.view?.hideLoader()
        self.view?.showAlert(title: "Error", message: "\(error)")
    }
    
    
}
