//
//  MainMenuInteractor.swift
//  MusicBattleChallenge
//
//  Created Manuel Rodríguez Torres on 13/10/20.
//  Copyright © 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class MainMenuInteractor: NSObject, MainMenuInteractorProtocol {
    func getImagesRequest() {
        RequestManager.generic(url: "https://picsum.photos/v2/list?page=2&limit=3", metodo: .get, tipoResultado: ImageEntity.self, delegate: self)
    }
    
    
    weak var output: MainMenuInteractorOutputProtocol?

}

extension MainMenuInteractor: RequestManagerDelegate{
    func onResponseSuccess(data: Decodable?, tag: Int) {
        guard let imageData = data as? ImageEntity else{
            output?.onErrorImagesResponse(error: .BAD_DECODABLE)
            return
        }
        output?.onSuccessImagesResponse(imageData: imageData)
    }
    func onResponseFailure(data: Decodable?, error: CodeResponse, tag: Int) {
        output?.onErrorImagesResponse(error: error)
    }
}
