//
//  MusicPlayerViewController.swift
//  MusicBattleChallenge
//
//  Created by Manuel Rodríguez Torres on 13/10/20.
//

import UIKit
import SDWebImage
import AVFoundation

class MusicPlayerViewController: UIViewController, MusicPlayerViewControllerProtocol {
    var presenter: MainMenuPresenterProtocol?
    
    var detail: ImageEntityElement?
    
    var idDetail: Int?
    
    var songPlayer = AVAudioPlayer()
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var songLabel: UILabel!
    var hasBeenPaused = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareSongAndSession()
        guard let musicDetail = self.detail else {
            return
        }
        self.songLabel.text = (musicDetail.song ?? "") + " by SepticFlesh"
        let imageUrl = URL(string: musicDetail.downloadURL ?? "")
        self.mainImage.showLoader()
        self.mainImage?.sd_setImage(with: imageUrl, placeholderImage: #imageLiteral(resourceName: "horns_icon"), completed: {_,_,_,_ in
            self.mainImage.hideLoader()
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.songPlayer.stop()
    }

    @IBAction func playSong(_ sender: Any) {
        self.songPlayer.play()
    }
    
    @IBAction func pauseSong(_ sender: Any) {
        if self.songPlayer.isPlaying{
            self.songPlayer.pause()
            self.hasBeenPaused = true
        } else {
            self.hasBeenPaused = false
        }
    }
    
    @IBAction func replaySong(_ sender: Any) {
        
        if self.songPlayer.isPlaying || self.hasBeenPaused {
            self.songPlayer.stop()
            self.songPlayer.currentTime = 0
            self.songPlayer.play()
        } else{
            self.songPlayer.play()
        }
        
    }
    //var songs = ["Dogma of Prometheus","Enemy of truth","Portrait of a headless man"]
    func prepareSongAndSession(){
        var songName = ""
        switch self.idDetail {
        case 0:
            songName = "dogma"
        case 1:
            songName = "enemy"
        case 2:
            songName = "headless"
        default:
            break
        }
        
        do {
            self.songPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: songName, ofType: "m4a")!))
            self.songPlayer.prepareToPlay()
            
            let audioSession = AVAudioSession.sharedInstance()
            do{
                try audioSession.setCategory(.playback)
            } catch let sessionError {
                print(sessionError)
            }
            
        } catch let songPlayerError {
            print(songPlayerError)
        }
    }
}
