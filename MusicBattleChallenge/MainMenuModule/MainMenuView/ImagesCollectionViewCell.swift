//
//  ImagesCollectionViewCell.swift
//  MusicBattleChallenge
//
//  Created by Manuel Rodríguez Torres on 13/10/20.
//

import UIKit
import SDWebImage
class ImagesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var imageAuthorLabel: UILabel!
    @IBOutlet weak var songNameLabel: UILabel!
    
    
    func fillCell(imageData: ImageEntityElement, song: String){
        
        guard let imageUrl = URL(string: imageData.downloadURL ?? "") else {
            return
        }
        self.showLoader()
        self.mainImage?.sd_setImage(with: imageUrl, placeholderImage: #imageLiteral(resourceName: "horns_icon"), completed: {_,_,_,_ in
            self.hideLoader()
        })
        self.imageAuthorLabel.text = imageData.author
        self.songNameLabel.text = song
    }
}
