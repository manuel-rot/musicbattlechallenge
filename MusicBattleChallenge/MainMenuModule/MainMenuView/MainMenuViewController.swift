//
//  MainMenuViewController.swift
//  MusicBattleChallenge
//
//  Created Manuel Rodríguez Torres on 13/10/20.
//  Copyright © 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDWebImage

class MainMenuViewController: UIViewController,MainMenuViewControllerProtocol {
    
    var imageDataArray: ImageEntity?
    
    var presenter: MainMenuPresenterProtocol?
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userDesciption: UILabel!
    
    var songs = ["Dogma of Prometheus","Enemy of Truth","Portrait of a Headless Man"]
    
    override func viewDidLoad() {
        self.presenter?.sendImageRequest()
        
        self.imagesCollectionView.delegate = self
        self.imagesCollectionView.dataSource = self
        
        let directory = self.getDocumentsDirectory().absoluteString + "profile.png"
        let urlPath = URL(string: directory)
        
        self.profileImage.sd_setImage(with: urlPath, placeholderImage: #imageLiteral(resourceName: "horns_icon"), completed: nil)
        
        self.userName.text = UserDefaults.standard.string(forKey: "profileName")
        self.userDesciption.text = UserDefaults.standard.string(forKey: "userDescription")
        
    }
    
    func reloadCollectionView() {
        self.imagesCollectionView.reloadData()
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
}

extension MainMenuViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.imageDataArray?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCollectionCell", for: indexPath) as! ImagesCollectionViewCell
        if let imageData = self.imageDataArray?[indexPath.row]{
            cell.fillCell(imageData: imageData, song: self.songs[indexPath.row])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard var detail = self.imageDataArray?[indexPath.row] else {
            return
        }
        detail.song = self.songs[indexPath.row]
        let vc = MainMenuRouter.createPlayer(detail: detail, id: indexPath.row)
        self.present(vc, animated: true, completion: nil)
    }
    
}

