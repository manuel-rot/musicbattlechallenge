//
//  MainMenuContracts.swift
//  MusicBattleChallenge
//
//  Created Manuel Rodríguez Torres on 13/10/20.
//  Copyright © 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
//Views
protocol MainMenuViewControllerProtocol: UIViewController {
    var presenter: MainMenuPresenterProtocol? {get set}
    var imageDataArray: ImageEntity? {get set}
    func reloadCollectionView()
}

protocol MusicPlayerViewControllerProtocol: UIViewController{
    var presenter: MainMenuPresenterProtocol? {get set}
    var detail: ImageEntityElement? { get set }
    var idDetail: Int? { get set }
}
//Interactor
protocol MainMenuInteractorProtocol: NSObject {
    var output: MainMenuInteractorOutputProtocol? {get set}
    func getImagesRequest()
}

protocol MainMenuInteractorOutputProtocol: NSObject {
    func onSuccessImagesResponse(imageData: ImageEntity)
    func onErrorImagesResponse(error: CodeResponse)
}

//Presenter
protocol MainMenuPresenterProtocol: NSObject {
    var view: MainMenuViewControllerProtocol? {get set}
    var interactor: MainMenuInteractorProtocol? {get set}
    var router: MainMenuRouterProtocol? {get set}
    var playerView: MusicPlayerViewControllerProtocol? { get set }
    func sendImageRequest()
}
//Router
protocol MainMenuRouterProtocol {
    var presenter: MainMenuPresenterProtocol? {get set}
    static func createModule() -> UIViewController
    static func createPlayer(detail: ImageEntityElement, id: Int) -> UIViewController
    
}

