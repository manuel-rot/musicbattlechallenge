//
//  ImagesEntity.swift
//  MusicBattleChallenge
//
//  Created by Manuel Rodríguez Torres on 13/10/20.
//

import Foundation
// MARK: - ImageEntityElement
struct ImageEntityElement: Codable {
    var id, author: String?
    var width, height: Int?
    var url, downloadURL: String?
    var song: String?

    enum CodingKeys: String, CodingKey {
        case id, author, width, height, url
        case downloadURL = "download_url"
    }
}

typealias ImageEntity = [ImageEntityElement]
