//
//  MainMenuRouter.swift
//  MusicBattleChallenge
//
//  Created Manuel Rodríguez Torres on 13/10/20.
//  Copyright © 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class MainMenuRouter: MainMenuRouterProtocol {
    static func createPlayer(detail: ImageEntityElement, id: Int) -> UIViewController {
        let s = mainstoryboard
        let view = s.instantiateViewController(withIdentifier: "MusicPlayerViewController") as! MusicPlayerViewControllerProtocol
        var presenter: MainMenuPresenterProtocol & MainMenuInteractorOutputProtocol = MainMenuPresenter()
        var interactor:MainMenuInteractorProtocol = MainMenuInteractor()
        var router: MainMenuRouterProtocol = MainMenuRouter()
        
        view.presenter = presenter
        view.detail = detail
        view.idDetail = id
        presenter.playerView = view
        presenter.router = router
        presenter.interactor = interactor
        router.presenter = presenter
        interactor.output = presenter
        
        return view
    }
    
    
    weak var presenter: MainMenuPresenterProtocol?
    
    static func createModule() -> UIViewController {
        let s = mainstoryboard
        let view = s.instantiateViewController(withIdentifier: "MainMenu") as! MainMenuViewControllerProtocol
        var presenter: MainMenuPresenterProtocol & MainMenuInteractorOutputProtocol = MainMenuPresenter()
        var interactor:MainMenuInteractorProtocol = MainMenuInteractor()
        var router: MainMenuRouterProtocol = MainMenuRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        router.presenter = presenter
        interactor.output = presenter
        
        return view
    }
    static var mainstoryboard: UIStoryboard {
        return UIStoryboard(name: "MainMenuStoryboard", bundle: nil)
    }
}
