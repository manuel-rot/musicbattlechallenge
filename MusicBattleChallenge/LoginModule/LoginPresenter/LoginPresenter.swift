//
//  LoginPresenter.swift
//  MusicBattleChallenge
//
//  Created Manuel Rodríguez Torres on 13/10/20.
//  Copyright © 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class LoginPresenter: NSObject, LoginPresenterProtocol {
    
    weak var view: LoginViewControllerProtocol?
    var interactor: LoginInteractorProtocol?
    var router: LoginRouterProtocol?
    
    func presentSignUpView(){
        let vc = SignUpRouter.createModule()
        self.view?.navigationController?.pushViewController(vc, animated: true)
    }

}
extension LoginPresenter: LoginInteractorOutputProtocol {
    
}
