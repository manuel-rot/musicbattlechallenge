//
//  LoginViewController.swift
//  MusicBattleChallenge
//
//  Created Manuel Rodríguez Torres on 13/10/20.
//  Copyright © 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,LoginViewControllerProtocol {
    
    var presenter: LoginPresenterProtocol?
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var userNameTextfield: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupHideKeyboardOnTap()
    }
    @IBAction func signUp(_ sender: Any) {
        self.presenter?.presentSignUpView()
    }
    @IBAction func signIn(_ sender: Any) {
        if UserDefaults.standard.string(forKey: "userPassword") == self.passwordTextField.text && UserDefaults.standard.string(forKey: "userName") == self.userNameTextfield.text{
            let vc = MainMenuRouter.createModule()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            self.showAlert(title: "Error", message: "Credenciales incorrectas")
        }
    }
}


