//
//  LoginRouter.swift
//  MusicBattleChallenge
//
//  Created Manuel Rodríguez Torres on 13/10/20.
//  Copyright © 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class LoginRouter: LoginRouterProtocol {
    
    weak var presenter: LoginPresenterProtocol?
    
    static func createModule() -> UIViewController {
        let s = mainstoryboard
        let view = s.instantiateViewController(withIdentifier: "Login") as! LoginViewControllerProtocol
        var presenter: LoginPresenterProtocol & LoginInteractorOutputProtocol = LoginPresenter()
        var interactor:LoginInteractorProtocol = LoginInteractor()
        var router: LoginRouterProtocol = LoginRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        router.presenter = presenter
        interactor.output = presenter
        
        let nav = UINavigationController.init(rootViewController: view)
        nav.modalPresentationStyle = .fullScreen
        nav.navigationBar.isHidden = true
        return nav
    }
    static var mainstoryboard: UIStoryboard {
        return UIStoryboard(name: "LoginStoryboard", bundle: nil)
    }
}
