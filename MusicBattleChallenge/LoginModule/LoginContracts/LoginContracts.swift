//
//  LoginContracts.swift
//  MusicBattleChallenge
//
//  Created Manuel Rodríguez Torres on 13/10/20.
//  Copyright © 2020 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
//Views
protocol LoginViewControllerProtocol: UIViewController {
    var presenter: LoginPresenterProtocol? {get set}
    
}
//Interactor
protocol LoginInteractorProtocol: NSObject {
    var output: LoginInteractorOutputProtocol? {get set}

}

protocol LoginInteractorOutputProtocol: NSObject {

}

//Presenter
protocol LoginPresenterProtocol: NSObject {
    var view: LoginViewControllerProtocol? {get set}
    var interactor: LoginInteractorProtocol? {get set}
    var router: LoginRouterProtocol? {get set}
    func presentSignUpView()
}
//Router
protocol LoginRouterProtocol {
    var presenter: LoginPresenterProtocol? {get set}
    static func createModule() -> UIViewController
    
}

