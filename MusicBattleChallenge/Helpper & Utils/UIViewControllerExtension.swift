//
//  UIViewControllerExtension.swift
//  MusicBattleChallenge
//
//  Created by Manuel Rodríguez Torres on 13/10/20.
//

import UIKit

extension UIViewController{
    
    func setupHideKeyboardOnTap() {
        self.view.addGestureRecognizer(self.endEditingRecognizer())
        self.navigationController?.navigationBar.addGestureRecognizer(self.endEditingRecognizer())
    }

    /// Dismisses the keyboard from self.view
    private func endEditingRecognizer() -> UIGestureRecognizer {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(self.view.endEditing(_:)))
        tap.cancelsTouchesInView = false
        return tap
    }
    
    func showAlert(title: String, message: String, action: ((Any)->Void)? = nil ){
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: action))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showLoader(){

//        let activityView = UIActivityIndicatorView(style: .whiteLarge)
//        activityView.center = self.view.center
//        self.view.addSubview(activityView)
//        activityView.startAnimating()

        var container: UIView = UIView()
        container.frame = self.view.frame
        container.center = self.view.center
        container.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)

        var loadingView: UIView = UIView()
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = self.view.center
        loadingView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10

        var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        actInd.style =
            UIActivityIndicatorView.Style.whiteLarge
        actInd.center = CGPoint(x: loadingView.frame.size.width / 2,
                                y: loadingView.frame.size.height / 2);
        loadingView.addSubview(actInd)
        
        actInd.translatesAutoresizingMaskIntoConstraints = false
        actInd.centerYAnchor.constraint(equalTo: loadingView.centerYAnchor).isActive = true
        actInd.centerXAnchor.constraint(equalTo: loadingView.centerXAnchor).isActive = true
        NSLayoutConstraint(item: actInd, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 80).isActive = true
               NSLayoutConstraint(item: actInd, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 80).isActive = true
        container.addSubview(loadingView)
        
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        loadingView.centerYAnchor.constraint(equalTo: container.centerYAnchor).isActive = true
        loadingView.centerXAnchor.constraint(equalTo: container.centerXAnchor).isActive = true
        NSLayoutConstraint(item: loadingView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 80).isActive = true
        NSLayoutConstraint(item: loadingView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 80).isActive = true

        self.view.addSubview(container)
        container.translatesAutoresizingMaskIntoConstraints = false
        container.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        container.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        container.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        container.heightAnchor.constraint(equalTo: self.view.heightAnchor).isActive = true
        
        self.view.layoutIfNeeded()
        
        actInd.startAnimating()
        
    }
    
    func hideLoader(){
        guard let viewContainer = self.view.subviews.last else{
            return
        }
        
        guard let indicator = viewContainer.subviews.last?.subviews.last as? UIActivityIndicatorView else{
            return
        }
        indicator.stopAnimating()
        indicator.removeFromSuperview()
        
        viewContainer.removeFromSuperview()
    }
    
}
