//
//  CustomUIView.swift
//  MusicBattleChallenge
//
//  Created by Manuel Rodríguez Torres on 13/10/20.
//

import UIKit

@IBDesignable class CustomUIView: UIView{
    @IBInspectable
    var shadowColor:UIColor = .black
    {
        didSet{
            layer.shadowColor = self.shadowColor.cgColor
        }
    }
    
    @IBInspectable
    var shadowOpacity:Float = 0
    {
        didSet{
            layer.shadowOpacity = self.shadowOpacity
        }
    }
    
    @IBInspectable
    var shadowRadius:CGFloat = 0{
        didSet{
            layer.shadowRadius = self.shadowRadius
        }
    }
    
    @IBInspectable
    var shadowOffset:CGSize = .zero
    {
        didSet{
            layer.shadowOffset = self.shadowOffset
        }
    }

    @IBInspectable
    var cornerRadius:CGFloat = 0
    {
        didSet{
            if cornerRadius < 0{
                layer.cornerRadius = bounds.height/2
            }
            else{
                layer.cornerRadius = self.cornerRadius
            }
        }
    }
    
    @IBInspectable
    var borderWidth:CGFloat = 1
    {
        didSet{
            layer.borderWidth = self.borderWidth
        }
    }
    
    @IBInspectable
    var borderColor:UIColor = .clear
    {
        didSet{
            layer.borderColor = self.borderColor.cgColor
        }
        
    }

    override func layoutSubviews() {
        if cornerRadius < 0{
            layer.cornerRadius = bounds.height/2
        }
        else{
            layer.cornerRadius = self.cornerRadius
        }
    }
}
