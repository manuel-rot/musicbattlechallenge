//
//  UIVIewExtension.swift
//  MusicBattleChallenge
//
//  Created by Manuel Rodríguez Torres on 13/10/20.
//
import UIKit
extension UIView{
    func showLoader(){

//        let activityView = UIActivityIndicatorView(style: .whiteLarge)
//        activityView.center = self.view.center
//        self.view.addSubview(activityView)
//        activityView.startAnimating()

        var container: UIView = UIView()
        container.frame = self.frame
        container.center = self.center
        container.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)

        var loadingView: UIView = UIView()
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = self.center
        loadingView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10

        var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        actInd.style =
            UIActivityIndicatorView.Style.whiteLarge
        actInd.center = CGPoint(x: loadingView.frame.size.width / 2,
                                y: loadingView.frame.size.height / 2);
        loadingView.addSubview(actInd)
        
        actInd.translatesAutoresizingMaskIntoConstraints = false
        actInd.centerYAnchor.constraint(equalTo: loadingView.centerYAnchor).isActive = true
        actInd.centerXAnchor.constraint(equalTo: loadingView.centerXAnchor).isActive = true
        NSLayoutConstraint(item: actInd, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 80).isActive = true
               NSLayoutConstraint(item: actInd, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 80).isActive = true
        container.addSubview(loadingView)
        
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        loadingView.centerYAnchor.constraint(equalTo: container.centerYAnchor).isActive = true
        loadingView.centerXAnchor.constraint(equalTo: container.centerXAnchor).isActive = true
        NSLayoutConstraint(item: loadingView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 80).isActive = true
        NSLayoutConstraint(item: loadingView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 80).isActive = true

        self.addSubview(container)
        container.translatesAutoresizingMaskIntoConstraints = false
        container.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        container.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        container.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        container.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        
        self.layoutIfNeeded()
        
        actInd.startAnimating()
        
    }
    
    func hideLoader(){
        guard let viewContainer = self.subviews.last else{
            return
        }
        
        guard let indicator = viewContainer.subviews.last?.subviews.last as? UIActivityIndicatorView else{
            return
        }
        indicator.stopAnimating()
        indicator.removeFromSuperview()
        
        viewContainer.removeFromSuperview()
    }
}
