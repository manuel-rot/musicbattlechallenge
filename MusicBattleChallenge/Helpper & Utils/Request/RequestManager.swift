//
//  RequestManager.swift
//  MusicBattleChallenge
//
//  Created by Manuel Rodríguez Torres on 13/10/20.
//

import Foundation
import Alamofire

class MultipartPayload {
    
    enum MimeTypes:String {
        case JPEG = "image/jpeg"
        case MOV  = "video/mov"
        case PDF  = "application/pdf"
    }
    
    var data: InputStream
    var name: String
    let mimeType: MimeTypes
    let fileName: String
    var length: UInt64
    
    init?(imagen:UIImage, name:String) {
        guard let d = imagen.jpegData(compressionQuality: 0.75) else {
            return nil
        }
        self.data = InputStream(data:  d)
        self.length = UInt64(d.count)
        self.name = name
        self.mimeType = .JPEG
        self.fileName = "image.jpeg"
    }
    init?(video:URL, name:String){
        guard let d = InputStream(url: video) else {
            return nil
        }
        do {
            guard let fileSize = try FileManager.default.attributesOfItem(atPath: video.path)[.size] as? NSNumber else {
                return nil
            }
            self.length = fileSize.uint64Value
        }
        catch {
            return nil
        }
        self.data = d
        self.name = name
        self.mimeType = .MOV
        self.fileName = "video.mov"
    }
    init?(pdf:URL, name:String){
        guard let d = InputStream(url: pdf) else {
            return nil
        }
        do {
            guard let fileSize = try FileManager.default.attributesOfItem(atPath: pdf.path)[.size] as? NSNumber else {
                return nil
            }
            self.length = fileSize.uint64Value
        }
        catch {
            return nil
        }
        self.data = d
        self.name = name
        self.mimeType = .PDF
        self.fileName = "archivo.pdf"
    }
}
/// Clase para realizar peticiones genericas
class RequestManager {
    typealias Metodo = HTTPMethod
    /// crea un request generico con la información proporcionada
    /// - Parameter url: Url a la cual se le hara la petición
    /// - Parameter metodo: Selección de metodo HTTP por utilizar
    /// - Parameter contenido: Contenido que llevara la petición en caso de ser nil no tendra contenido, default nil
    /// - Parameter tipoResultado: Tipo de encodable utilizado para la respuesta, en caso de ser nil, se regresara un success con contenido vacio, default nil
    /// - Parameter delegate: Delegado que recibira las respuestas de la petición
    /// - Parameter tag: Tag que permite operaciones de tipo bandera
    static func generic<A:Encodable, T:Decodable>(url:String, metodo:Metodo, contenido:A?, tipoResultado:T.Type?, delegate:RequestManagerDelegate,headers: [String:String]? = nil, tag:Int = 0,multipartsPayload:[MultipartPayload]? = nil) {
        guard let urlForRequest = URL(string: url) else {
            print("WRONG URL \(url)")
            delegate.onResponseFailure(data: nil, error: .BAD_URL, tag: tag)
            return
        }
        guard NetworkReachabilityManager()!.isReachable else {
            delegate.onResponseFailure(data: nil, error: .NOT_CONNECTION, tag: tag)
            return
        }
        var urlRequest:URLRequest = URLRequest(url: urlForRequest)
        urlRequest.httpMethod = metodo.rawValue
        if headers == nil{
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        
        for header in headers ?? [:]{
            urlRequest.setValue(header.value, forHTTPHeaderField: header.key)
        }
        let dataContenido:Data?
        if let contenido = contenido {
            dataContenido = try? JSONEncoder().encode(contenido)
        } else { dataContenido = nil }
        
        let accion:((_ data: DataResponse<Data>) -> (Void)) = { (data: DataResponse<Data>) -> (Void)  in
            
            switch data.result {
            case .success:
                guard let response = data.response, let value = data.data else {
                    delegate.onResponseFailure(data: nil, error: .UNKNOW, tag: tag)
                    return
                }
                
                let code = CodeResponse(rawValue: response.statusCode) ?? CodeResponse.UNKNOW
                if code == .UNKNOW { print("Code not implemented \(response.statusCode)") }
                if code == .OK {
                    do {
                        let obj = tipoResultado == nil ? nil : try JSONDecoder().decode(tipoResultado!.self, from: value)
                        delegate.onResponseSuccess(data: obj, tag: tag)
                    }
                    catch let jsonError{
                        print("Falied to decode Json: ",jsonError)
                        delegate.onResponseFailure(data: nil, error: .BAD_DECODABLE, tag: tag)
                    }
                }
                else if code == .SIN_CONTENIDO{
                    delegate.onResponseSuccess(data: nil, tag: tag)
                }
                    
                else {
                    print("Some code \(response.statusCode)")
                    //delegate.onResponseFailure(error: code, tag: tag)
                    var obj:Decodable? = tipoResultado == nil ? nil : try? JSONDecoder().decode(tipoResultado!.self, from: value)
                    if obj == nil {
                        let obj2: [String:String]? = try? JSONDecoder().decode([String:String].self, from: value)
                        obj = obj2
                    }
                    delegate.onResponseFailure(data: obj, error: code, tag: tag)
                    print(String.init(data: value, encoding: .utf8) ?? "UNPARSER")
                }
            case .failure:
                delegate.onResponseFailure(data: nil, error: .UNKNOW, tag: tag)
            }
        }
        guard let mp = multipartsPayload, !mp.isEmpty else {
            urlRequest.httpBody = dataContenido
            Alamofire.request(urlRequest).responseData( completionHandler: accion)
            return
        }
        Alamofire.upload(multipartFormData: { (multipart) in
            if let dataContenido = dataContenido {
                multipart.append(dataContenido, withName:"data")
            }
            mp.forEach { (value:MultipartPayload) in
                multipart.append(value.data, withLength: value.length, name: value.name, fileName: value.fileName, mimeType: value.mimeType.rawValue)
            }
            print(multipart)
        },with: urlRequest){ result in
            switch result{
             case .success(let upload,_, _):
                upload.responseData(completionHandler:accion)
             case .failure(_):
                 delegate.onResponseFailure(data: nil, error: .UNKNOW, tag: tag)
             }
        }
        
    }
   
    static func generic(url:String, metodo:Metodo, delegate:RequestManagerDelegate,headers: [String:String]?, tag:Int = 0, multipartsPayload:[MultipartPayload]? = nil) {
        let c : DummyCodable? = nil
        let t : DummyCodable.Type? = nil
        self.generic(url: url, metodo: metodo, contenido: c, tipoResultado: t, delegate: delegate,headers: headers, tag: tag, multipartsPayload:multipartsPayload)
    }
    static func generic<A:Encodable>(url:String, metodo:Metodo, contenido:A?, delegate:RequestManagerDelegate,headers: [String:String]? = nil, tag:Int = 0, multipartsPayload:[MultipartPayload]? = nil){
        let t : DummyCodable.Type? = nil
        self.generic(url: url, metodo: metodo, contenido: contenido, tipoResultado: t, delegate: delegate,headers: headers, tag: tag, multipartsPayload: multipartsPayload)
    }
    static func generic<T:Decodable>(url:String, metodo:Metodo, tipoResultado:T.Type?, delegate:RequestManagerDelegate,headers: [String:String]? = nil, tag:Int = 0, multipartsPayload:[MultipartPayload]? = nil) {
        let c : DummyCodable? = nil
        self.generic(url: url, metodo: metodo, contenido: c, tipoResultado: tipoResultado, delegate: delegate,headers: headers ,tag: tag, multipartsPayload: multipartsPayload)
    }
}
fileprivate class DummyCodable:Codable {}
/// Delegado del Reques manager para recibir la peticiones
protocol RequestManagerDelegate {
    /// Respuesta en caso de exito
    /// - Parameter data: Información que previamente esta Decodeada en el tipo previamente solicitado
    /// - Parameter tag: Tag que permite operaciones de tipo banderas
    func onResponseSuccess(data:Decodable?,tag:Int)
    /// Respuesta en caso de error
    /// - Parameter error: Codigo de error recibido por el servidor o alguno customizado, default Unknow
    /// - Parameter tag: Tag que permite operaciones de tipo banderas
    //func onResponseFailure(error:CodeResponse,tag:Int)
    func onResponseFailure(data:Decodable?, error:CodeResponse, tag:Int)
}

extension RequestManagerDelegate {
    func onResponseSuccess(data:Decodable?,tag:Int) {
        print("onResponseSuccessful(data:Decodable?,tag:\(tag)) not implemented")
    }

    func onResponseFailure(data:Decodable?, error:CodeResponse, tag:Int){
        
    }
}
enum CodeResponse: Int {
    case OK = 200
    case SIN_CONTENIDO = 204
    case AUTHENTICATION_FAILED = 409
    case NOT_FOUND = 404
    case INVALID_TOKEN = 401
    case ERROR_SERVER = 500
    case UNKNOW = -1
    case NOT_CONNECTION = -2
    case BAD_URL = -3
    case BAD_DECODABLE = -4
}

